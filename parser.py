#!/usr/bin/python3

import json
import re
from event import Event
from os import walk

# The path to files containg the json from wikipedia
WIKI_FILES_DIR = "/home/james/Dropbox/Collection/McMinn2013/Wikipedia/events_by_day"

f = []
for (dirpath, dirname, filenames) in walk(WIKI_FILES_DIR):
	f.extend(filenames)
	break

days = []
for filename in f:
	with open("%s/%s" % (WIKI_FILES_DIR, filename), 'r') as json_file:
		jsondata = json.load(json_file)
		for (key, value) in jsondata['query']['pages'].items():
			days.append(value['revisions'][0]['*'])

top_event_regex = re.compile("\[\[(.*)\]\]\:$")
sub_event_regex = re.compile("\[\[(.*)\]\]\:")
page_link_regex = re.compile("\[\[(.*?)\]\]")
external_regex = re.compile("[^\[]\[([^\[].*?)\(([^\)]*?)\)\]")
date_regex = re.compile("\{\{.*\|([0-9]{4})\|([0-9]{1,2})\|([0-9]{1,2})\}\}")
# {{Current events header|2012|11|06}}

category = None

events = []
count = 0
for day in days:
	for line in map(str.strip, day.split('\n')):
		if len(line) == 0:
			continue

		result = date_regex.match(line)
		if result:
			date = ("%s-%s-%s" % (result.group(1), result.group(2), result.group(3)))

		if line[0] == ';':
			category = line[1:]

		elif line[0] == '*':
			count += 1
			line = line[1:]

			# Top level "big" events covering multiple sub-events
			result = top_event_regex.match(line)
			if result:
				top_event = result.group(1).split("|")[0]
				continue

			if line[0] != '*':
				top_event = None
			else:
				line = line[1:]

			# Top level "mini" events covering a single sub-event
			result = sub_event_regex.match(line)
			if result:
				sub_event = result.group(1).split("|")[0]
				line = line[result.end(0):].strip()
			else:
				sub_event = None

			# Extract reference URLs and remove them from the description
			urls = []
			refs_start = None
			for match in re.finditer(external_regex, line):
				if refs_start is None:
					refs_start = match.start(0)
				urls.append(match.group(1).strip())
			if refs_start:
				line = line[:refs_start]

			# Replace markdown with raw text and extract any wiki entities
			entities = []
			diff = 0
			for match in re.finditer(page_link_regex, line):
				parts = match.group(1).split("|")
				entities.append(parts[0])
				if len(parts) > 1:
					text_entity = parts[1]
				else:
					text_entity = parts[0]

				line = line[:match.start(0)-diff] + text_entity + line[match.end(0)-diff:]
				diff += len(match.group(0)) - len(text_entity)

			# Make sure that if there is a parent event, the highest level is always filled
			if top_event is None and sub_event is not None:
				top_event = sub_event
				sub_event = None

			event = Event(category, top_event, sub_event, urls, entities, date, line)
			events.append(event)


print(json.dumps([event for event in map(lambda x : x.__dict__, events)]))

print(len(events))
print(count)