class Event:

	def __init__(self, category, top_event, sub_event, urls, entities, date, description):
		self.category = category
		self.top_event = top_event
		self.sub_event = sub_event
		self.description = description
		self.entities = entities
		self.date = date
		self.urls = urls

	def reprJSON(self):
		return dict(category = self.category, description = self.description)

	def __str__(self):
		return "Date     :\t%s\nCategory :\t%s\nTop Event:\t%s\nSub Event:\t%s\nURLs     :\t%s\nEntities:\t%s\nDescript.:\t%s\n" \
			% (self.date, self.category, self.top_event, self.sub_event, self.urls, self.entities, self.description)